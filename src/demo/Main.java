package demo;

import java.util.*;
import java.nio.*;
import java.nio.file.*;
import java.sql.*;
import java.util.Scanner;

public class Main {
	private String Task_ID;
	private String Skill;
	
	public static void main(String[] args) {
		
		try {
			List<String> lines = Files.readAllLines(Paths.get("src\\data\\task.csv"));
			for (String line : lines) {
				//line = line.replace("\"", "");
				String []result = line.split(",");
				for(String s : result)
					System.out.print(s + " ");
				System.out.println();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		
		try {
			List<String> lines = Files.readAllLines(Paths.get("src\\data\\team.csv"));
			for (String line : lines) {
				//line = line.replace("\"", "");
				String []result = line.split(",");
				for(String s : result)
					System.out.print(s + " ");
				System.out.println();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		
		try {
			List<String> lines = Files.readAllLines(Paths.get("src\\data\\team_skill.csv"));
			for (String line : lines) {
				//line = line.replace("\"", "");
				String []result = line.split(",");
				for(String s : result)
					System.out.print(s + " ");
				System.out.println();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
}
